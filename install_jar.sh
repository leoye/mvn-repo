#!/bin/bash
 
PATH_TO_VERTX_JARS=/Users/leoye/MacDev/Docs/Jihuobao/kaptcha-2.3.2
GROUP_ID=com.google.code.kaptcha
VERSION=2.3.2
LIBS=(kaptcha)
 
for NAME in ${LIBS[*]}
do
    printf "Adding %s-%s.jar to local repository...\n" $NAME $VERSION
    mvn install:install-file -DgroupId=$GROUP_ID -DartifactId=$NAME -Dversion=$VERSION -Dfile=$PATH_TO_VERTX_JARS/$NAME-$VERSION.jar -Dpackaging=jar -DgeneratePom=true  -DcreateChecksum=true -DlocalRepositoryPath=./
    if [ -f $PATH_TO_VERTX_JARS/$NAME-$VERSION-source.jar ]
   	then
	    mvn install:install-file -DgroupId=$GROUP_ID -DartifactId=$NAME -Dclassifier=sources -Dversion=$VERSION -Dfile=$PATH_TO_VERTX_JARS/$NAME-$VERSION-source.jar -Dpackaging=jar -DcreateChecksum=true -DlocalRepositoryPath=./
    fi
done